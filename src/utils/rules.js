/* eslint-disable */

const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
const hexRegex = /^[0-9A-F]+$/

export default {
  isRequired: v => !!v || 'Field cannot be empty',
  isEmail: v => emailRegex.test(v) || 'Invalid Email',
  minCharacters: count => (v => !!v && v.length >= count || 
  `Must be at least ${count} characters`),
  maxCharacters: count => (v => !!v && v.length <= count || 
  `Can be a maximum of ${count} characters`),
  exactCharacters: count => (v => !!v && v.length === count || 
  `Must be exactly ${count} characters`),
  isNumeric: v => /^[0-9]+$/.test(v) || 'Must be numbers only',
  isInteger: v => !isNaN(parseInt(v)) || 'Must be an integer only',
  isFloat: v => !isNaN(parseFloat(v)) || 'Must be a decimal number',
  isNonZero: v => (!isNaN(parseInt(v)) && parseInt(v) > 0) || 'Must be a number > 0',
  isHour: v => /^(0[0-9]|1[0-9]|2[0-3])\:[0-5]\d\:[0-5]\d$/.test(v) || 'Must be in HH:MM:SS format',
  isHourMinutes: v => /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(v) || 'Must be in HH:MM format',
  isURL: v => urlRegex.test(v) || 'Invalid URL',
  isRank: v => !isNaN(parseInt(v)) && parseInt(v) >= 1 || 'Must be an integer > 1',
  maxNumber: max => (v => !isNaN(parseFloat(v)) && parseFloat(v)<= max ||
  `Must be maximum ${max}`),
  minNumber: min => (v => !isNaN(parseFloat(v)) && parseFloat(v)>= min ||
  `Must be minimum ${min}`),
  isHex: v => hexRegex.test(v) || 'Must be a hexadecimal string',
  nonEmptyArray: v => (Array.isArray(v) && v.length > 0) || 'List cannot be empty',
  isEqual: (ref, fieldName) => (v => v == ref || `Must be same as ${fieldName}`),
  timeAfter,
  timeBefore,
  dateAfter,
  dateBefore
}

function timeAfter (date, dateRef, timeRef) {
  return v => {
    // Prepare base moment
    const base = moment(`${date} ${v}`, 'YYYY-MM-DD HH:mm')
    // Prepare ref moment
    const ref = moment(`${dateRef} ${timeRef}`, 'YYYY-MM-DD HH:mm')
    return base.isAfter(ref) || `Must be after ${dateRef} ${timeRef}`
  }
}

function timeBefore (date, dateRef, timeRef) {
  return v => {
    // Prepare base moment
    const base = moment(`${date} ${v}`, 'YYYY-MM-DD HH:mm')
    // Prepare ref moment
    const ref = moment(`${dateRef} ${timeRef}`, 'YYYY-MM-DD HH:mm')
    return base.isBefore(ref) || `Must be before ${dateRef} ${timeRef}`
  }
}

function dateAfter (time, dateRef, timeRef) {
  return v => {
    // Prepare base moment
    const base = moment(`${v} ${time}`, 'YYYY-MM-DD HH:mm')
    // Prepare ref moment
    const ref = moment(`${dateRef} ${timeRef}`, 'YYYY-MM-DD HH:mm')
    return base.isAfter(ref) || `Must be after ${dateRef} ${timeRef}`
  }
}

function dateBefore (time, dateRef, timeRef) {
  return v => {
    // Prepare base moment
    const base = moment(`${v} ${time}`, 'YYYY-MM-DD HH:mm')
    // Prepare ref moment
    const ref = moment(`${dateRef} ${timeRef}`, 'YYYY-MM-DD HH:mm')
    return base.isBefore(ref) || `Must be before ${dateRef} ${timeRef}`
  }
}