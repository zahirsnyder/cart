import rules from '../utils/rules'

export default {
  props: {
    model: [String, Number, Boolean, Array, Object],
    label: { type: String, required: true },
    type: { type: String, default: 'text' },
    outlined: { type: Boolean, default: true },
    filled: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    placeholder: String,
    hint: String,
    persistentHint: Boolean,
    email: Boolean,
    required: Boolean,
    min: [Number, String],
    max: [Number, String],
    minChars: [Number, String],
    maxChars: [Number, String],
    onlyChars: [Number, String],
    numeric: Boolean,
    integer: Boolean,
    float: Boolean,
    nonZero: Boolean,
    hour: Boolean,
    hourMins: Boolean,
    url: Boolean,
    rank: Boolean,
    hex: Boolean,
    array: Boolean,
    equal: Boolean,
    compare: [Number, String, Object, Array, Boolean],
    fieldName: String,
    timeAfter: Boolean,
    timeBefore: Boolean,
    dateAfter: Boolean,
    dateBefore: Boolean,
    date: String,
    time: String,
    dateRef: String,
    timeRef: String,
    step: [Number, String]
  },
  data () {
    return {
      value: ''
    }
  },
  watch: {
    model (updated) {
      this.value = updated
    }
  },
  computed: {
    rules () {
      let out = []
      if (this.email) out.push(rules.isEmail)
      if (this.required) out.push(rules.isRequired)
      if (this.min != undefined) out.push(rules.minNumber(this.min))
      if (this.max != undefined) out.push(rules.maxNumber(this.max))
      if (this.minChars) out.push(rules.minCharacters(this.minChars))
      if (this.maxChars) out.push(rules.maxCharacters(this.maxChars))
      if (this.onlyChars) out.push(rules.exactCharacters(this.onlyChars))
      if (this.numeric) out.push(rules.isNumeric)
      if (this.integer) out.push(rules.isInteger)
      if (this.float) out.push(rules.isFloat)
      if (this.nonZero) out.push(rules.isNonZero)
      if (this.hour) out.push(rules.isHour)
      if (this.hourMins) out.push(rules.isHourMinutes)
      if (this.url) out.push(rules.isURL)
      if (this.rank) out.push(rules.isRank)
      if (this.hex) out.push(rules.isHex)
      if (this.array) out.push(rules.nonEmptyArray)
      if (this.equal && this.compare && this.fieldName)
      out.push(rules.isEqual(this.compare, this.fieldName))
      if (this.timeAfter && this.date && this.dateRef && this.timeRef)
      out.push(rules.timeAfter(this.date, this.dateRef, this.timeRef))
      if (this.timeBefore && this.date && this.dateRef && this.timeRef)
      out.push(rules.timeBefore(this.date, this.dateRef, this.timeRef))
      if (this.dateAfter && this.time && this.dateRef && this.timeRef)
      out.push(rules.dateAfter(this.time, this.dateRef, this.timeRef))
      if (this.dateBefore && this.time && this.dateRef && this.timeRef)
      out.push(rules.dateBefore(this.time, this.dateRef, this.timeRef))
      return out
    }
  },
  methods: {
    sync () {
      this.$emit('update:model', this.value)
    }
  },
  created () {
    this.value = this.model
  }
}