import rules from "../utils/rules";

const booleans = [
  { text: 'Yes', value: true },
  { text: 'No', value: false }
]

export default {
  computed: {
    rules: () => rules,
    boolOptions: () => booleans
  }
}